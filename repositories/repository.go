package repositories

var RoleRepo = NewRoleRepository()
var UserRepo = NewUserRepository()
var CategoryRepo = NewCategoryRepository()
var ProductRepo = NewProductRepository()
var WarehouseRepo = NewWarehouseRepository()
var QuantityRepo = NewQuantityRepository()
var OrderRepo = NewOrderRepository()
