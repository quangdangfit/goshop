package routers

import (
	"goshop/services"
)

var roleService = services.RoleSer
var userService = services.UserSer
var categoryService = services.CategorySer
var productService = services.ProductSer
var warehouseService = services.WarehouseSer
var quantityService = services.QuantitySer
var orderService = services.OrderSer
