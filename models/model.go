package models

const (
	OrderStatusNew      = "new"
	OrderStatusAssigned = "assigned"
	OrderStatusDone     = "done"
	OrderStatusCanceled = "canceled"
)
